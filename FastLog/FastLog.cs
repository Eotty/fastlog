﻿using System;
using System.IO;


namespace FastLog
{
    public class FastLog
    {

        private static void AppendLineToLogAndConsole(string lines)
        {
            lines = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " =: " + lines;
            var path = @"C:\Users\Public\TestFolder\" + DateTime.Now.ToShortDateString() + "-HTMLOutput.txt";
            
            Console.WriteLine(lines);

            if (!File.Exists(path))
            {
                File.Create(path).Dispose();
            }

            TextWriter tw = new StreamWriter(path, true);
            tw.WriteLine(lines);
            tw.Close();
        }
    }
}
